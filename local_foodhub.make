

api = 2
core = 7.x


;projects[views_bulk_operations][version] = "3.1"
;projects[][subdir] = contrib

projects[addressfield][version] = "1.0-beta4"
projects[addressfield][subdir] = contrib

;projects[apps][version] = "1.0-beta7"
;projects[][subdir] = contrib
;projects[apps][patches][] = "http://drupal.org/files/1790902-check-last-modified-existing.patch"

;projects[ctools][version] = "1.3"
;projects[][subdir] = contrib

projects[button_field][version] = "1.0-beta1"
projects[button_field][subdir] = contrib

projects[caption_filter][version] = "1.2"
projects[caption_filter][subdir] = contrib
projects[caption_filter][patches][] = "http://drupal.org/files/1432092-button-and-image.patch"

projects[commerce][version] = "1.7"
projects[commerce][subdir] = contrib

projects[commerce_autosku][version] = "1.1"
projects[commerce_autosku][subdir] = contrib

projects[commerce_cod][version] = "1.0"
projects[commerce_cod][subdir] = contrib

projects[commerce_decimal_quantities][version] = "1.0"
projects[commerce_decimal_quantities][subdir] = contrib

projects[commerce_features][version] = "1.0-rc1"
projects[commerce_features][subdir] = contrib

projects[commerce_feeds][version] = "1.3"
projects[commerce_feeds][subdir] = contrib

projects[commerce_fieldgroup_panes][version] = "1.0"
projects[commerce_fieldgroup_panes][subdir] = contrib

projects[commerce_invoice_receipt][version] = "1.0"
projects[commerce_invoice_receipt][subdir] = contrib

projects[commerce_reorder][version] = "1.1"
projects[commerce_reorder][subdir] = contrib

projects[commerce_search_api][version] = "1.2"
projects[commerce_search_api][subdir] = contrib

projects[commerce_vbo_views][version] = "1.2"
projects[commerce_vbo_views][subdir] = contrib

projects[computed_field][version] = "1.0-beta1"
projects[computed_field][subdir] = contrib

projects[context][version] = "3.0-beta6"
projects[context][subdir] = contrib

;projects[facetapi][version] = "1.3"
;projects[][subdir] = contrib

;projects[date][version] = "2.6"
;projects[][subdir] = contrib

;projects[defaultcontent][version] = "1.0-alpha9"
;projects[defaultcontent][patches][] = "http://drupal.org/files/1757782-cannot-import-menu-hierarchy-8.patch"

;projects[devel][version] = "1.3"
;projects[][subdir] = contrib

projects[email][version] = "1.2"
projects[email][subdir] = contrib

projects[emogrifier][version] = "1.18"
projects[emogrifier][subdir] = contrib

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
;libraries[emogrifier][download][type] = "zip"
;libraries[emogrifier][download][url] = "http://www.pelagodesign.com/emogrifier/emogrifier.zip"
;libraries[emogrifier][directory_name] = "emogrifier"
;libraries[emogrifier][type] = "library"

; Modules
;projects[entity][version] = "1.0"
;projects[][subdir] = contrib

;projects[entityreference][version] = "1.0"
;projects[][subdir] = contrib

projects[rb][version] = "1.x-dev"
projects[rb][subdir] = contrib

;projects[features][version] = "2.0-beta2"
;projects[][subdir] = contrib

projects[feeds][version] = "2.0-alpha8"
projects[feeds][subdir] = contrib

projects[feeds_tamper][version] = "1.0-beta4"
projects[feeds_tamper][subdir] = contrib

;projects[field_group][version] = "1.1"
;projects[][subdir] = contrib

;projects[file_entity][version] = "2.0-unstable7+61-dev"
;projects[][subdir] = contrib

projects[geocoder][version] = "1.2"
projects[geocoder][subdir] = contrib

projects[geofield][version] = 7.x-2.0-alpha2
projects[geofield][subdir] = contrib

projects[geophp][version] = "1.7"
projects[geophp][subdir] = contrib

projects[inline_entity_form][version] = "1.2"
projects[inline_entity_form][subdir] = contrib

projects[job_scheduler][version] = "2.0-alpha3"
projects[job_scheduler][subdir] = contrib

projects[mailsystem][version] = "2.34"
projects[mailsystem][subdir] = contrib

projects[mimemail][version] = "1.0-alpha2"
projects[mimemail][subdir] = contrib

projects[openlayers][version] = "2.0-beta6"
projects[openlayers][subdir] = contrib

projects[print][version] = "1.2"
projects[print][subdir] = contrib

projects[proj4js][version] = "1.2"
projects[proj4js][subdir] = contrib

projects[references][version] = 2.1
projects[references][subdir] = contrib

projects[rules][version] = "2.3"
projects[rules][subdir] = contrib

;projects[search_api][version] = "1.5"
;projects[][subdir] = contrib

;projects[search_api_db][version] = "1.0-rc1"
;projects[][subdir] = contrib

projects[search_api_ranges][version] = "1.4"
projects[search_api_ranges][subdir] = contrib

;projects[search_api_solr][version] = "1.0-rc3"
;projects[][subdir] = contrib
;projects[search_api_solr][patches][] = "http://drupal.org/files/1407282-variable_solr_connection_class-37.patch"

;projects[tablefield][version] = "2.1"
;projects[][subdir] = contrib

;projects[token][version] = "1.5"
;projects[][subdir] = contrib

projects[twitter_username][version] = "1.4"
projects[twitter_username][subdir] = contrib

;projects[uuid][version] = "1.0-alpha3+47-dev"
;projects[][subdir] = contrib
;projects[uuid][patches][] = "http://drupal.org/files/1605284-define-types-for-tokens-6.patch"

projects[viewfield][version] = "2.0"
projects[viewfield][subdir] = contrib

;projects[views][version] = "3.7"
;projects[][subdir] = contrib

projects[views_pdf][version] = "1.0"
projects[views_pdf][subdir] = contrib

;projects[views_autocomplete_filters][version] = "1.0-beta2"
;projects[][subdir] = contrib

projects[views_calc][version] = "1.0"
projects[views_calc][subdir] = contrib

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[fpdi][download][type] = get
libraries[fpdi][download][url] = "http://www.setasign.de/supra/kon2_dl/60111/FPDI-1.4.4.zip"
libraries[fpdi][directory_name] = "fpdi"
;libraries[fpdi][type] = "library"

libraries[fpdi][download][type] = get
libraries[fpdi][download][url] = http://www.setasign.de/supra/kon2_dl/63411/FPDF_TPL-1.2.3.zip
libraries[fpdi][directory_name] = fpdi_tpl
;libraries[fpdi][type] = library

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[tcpdf][download][type] = get
libraries[tcpdf][download][url] = http://downloads.sourceforge.net/project/tcpdf/tcpdf_6_0_018.zip
libraries[tcpdf][directory_name] = "tcpdf"
;libraries[tcpdf][type] = "library"


