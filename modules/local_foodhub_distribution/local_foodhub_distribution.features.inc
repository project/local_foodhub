<?php
/**
 * @file
 * local_foodhub_distribution.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function local_foodhub_distribution_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function local_foodhub_distribution_node_info() {
  $items = array(
    'distribution_point' => array(
      'name' => t('Distribution point'),
      'base' => 'node_content',
      'description' => t('A place where foodhub orders can be collected.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
