<?php

/**
 * Implements hook_rules_action_info().
 */
function foodhub_rules_action_info() {
  $actions = array();

  $actions['foodhub_format_line_items'] = array(
    'label' => t('Formats a list of line items'),
    'parameter' => array(
      'commerce_line_item_list' => array(
        'type' => 'list',
        'label' => t('Line items list'),
        'description' => t('Line items'),
      ),
    ),
    'provides' => array(
      'line_items_html' => array(
        'type' => 'text',
        'label' => t('Line items'),
        'save' => TRUE,
      ),
    ),
    'group' => t('Foodhub'),
  );

  return $actions;
}


/**
 * Rules action:
 *
 * @param $commerce_line_item_list object
 *   The order object
 */
function foodhub_format_line_items($commerce_line_item_list) {
  $product_totals = array();
  
  foreach ($commerce_line_item_list as $value) {
    dpm($value);
    $product_id = $value->commerce_product['und']['0']['product_id'];
    dpm($product_id);
    // dpm($value->commerce_product['und']['0']['product_id']);
    // $product = array_values(entity_load('commerce_product', array($product_id)));
    // $product = $product[0];
    // dpm($product->title);
    // kpr($product);

    // Add up the quantities
    if (! array_key_exists($product_id, $product_totals)) {
      // Set total and make integer
      $product_totals[$product_id] = $value->quantity + 0;
    } else {
      $product_totals[$product_id] = $product_totals[$product_id] + $value->quantity;
    }
    // $txt = $txt . ', ' . $value->quantity;
    
  }
  // kpr($product_totals);

  $txt = '<table>';
  foreach ($product_totals as $prod_id => $quantity) {
    $product = array_values(entity_load('commerce_product', array($prod_id)));
    $product = $product[0];
    $txt = "$txt <tr><td>$product->title</td><td>$quantity</td></tr>";
  }
  $txt = $txt . '</table>';
    
  // dpm($txt);
  return array('line_items_html' => $txt);
}



/**
 * Rules action:
 *
 * @param $order object
 *   The order object
 */
function foodhub_initialise_recurring($order) {
	global $user;
  $account = $user;

  // Get the line items of the order.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
      $product = $line_item_wrapper->commerce_product->value();
      if ($product->status) {
        $line_item = $line_item_wrapper->value();

        // Generate a line item product based in the current one.
        $new_line_item = commerce_product_line_item_new($product, $line_item->quantity, $line_item->order_id, $line_item->data, $line_item->type);

        // Merge both line items to get the fields (if any).
        $new_line_item = (object) array_merge((array) $line_item, (array) $new_line_item);

        // @TODO Add option to combine / add sepparately.
        // See @commerce_cart_product_add
        commerce_cart_product_add($account->uid, $new_line_item);
      }
      else {
        drupal_set_message(t('Some products weren\'t copied to the cart as they aren\'t currently available'), 'status', FALSE);
      }
    }
    else {
      drupal_set_message(t('Some products weren\'t copied to the cart as they aren\'t currently available'), 'status', FALSE);
    }
  }
}

