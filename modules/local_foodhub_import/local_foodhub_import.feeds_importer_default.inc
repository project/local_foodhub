<?php
/**
 * @file
 * local_foodhub_import.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function local_foodhub_import_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'foodhub_product_display_importer';
  $feeds_importer->config = array(
    'name' => 'Foodhub product display importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'foodhub_product_display',
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Product Name',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Description',
            'target' => 'field_product_description',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'SKU',
            'target' => 'field_product:sku',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['foodhub_product_display_importer'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'foodhub_product_importer';
  $feeds_importer->config = array(
    'name' => 'Foodhub product importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsCommerceProductProcessor',
      'config' => array(
        'product_type' => 'foodhub_product',
        'author' => 0,
        'tax_rate' => '',
        'mappings' => array(
          0 => array(
            'source' => 'Product Name',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'SKU',
            'target' => 'sku',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'Category',
            'target' => 'field_category',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Organic',
            'target' => 'field_organic',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Description',
            'target' => 'field_description',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Unit HUB Price',
            'target' => 'commerce_price:amount',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Unit',
            'target' => 'field_unit',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'price comparison',
            'target' => 'field_price_comparison',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['foodhub_product_importer'] = $feeds_importer;

  return $export;
}
