<?php
/**
 * @file
 * local_foodhub_import.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function local_foodhub_import_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer feeds.
  $permissions['administer feeds'] = array(
    'name' => 'administer feeds',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds',
  );

  // Exported permission: administer feeds_tamper.
  $permissions['administer feeds_tamper'] = array(
    'name' => 'administer feeds_tamper',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds_tamper',
  );

  // Exported permission: clear foodhub_product_display_importer feeds.
  $permissions['clear foodhub_product_display_importer feeds'] = array(
    'name' => 'clear foodhub_product_display_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: clear foodhub_product_importer feeds.
  $permissions['clear foodhub_product_importer feeds'] = array(
    'name' => 'clear foodhub_product_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: import foodhub_product_display_importer feeds.
  $permissions['import foodhub_product_display_importer feeds'] = array(
    'name' => 'import foodhub_product_display_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: import foodhub_product_importer feeds.
  $permissions['import foodhub_product_importer feeds'] = array(
    'name' => 'import foodhub_product_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: import user feeds.
  // $permissions['import user feeds'] = array(
  //   'name' => 'import user feeds',
  //   'roles' => array(
  //     'administrator' => 'administrator',
  //   ),
  //   'module' => 'feeds',
  // );

  // Exported permission: tamper foodhub_product_display_importer.
  $permissions['tamper foodhub_product_display_importer'] = array(
    'name' => 'tamper foodhub_product_display_importer',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: tamper foodhub_product_importer.
  $permissions['tamper foodhub_product_importer'] = array(
    'name' => 'tamper foodhub_product_importer',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds_tamper',
  );

  // Exported permission: unlock foodhub_product_display_importer feeds.
  $permissions['unlock foodhub_product_display_importer feeds'] = array(
    'name' => 'unlock foodhub_product_display_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: unlock foodhub_product_importer feeds.
  $permissions['unlock foodhub_product_importer feeds'] = array(
    'name' => 'unlock foodhub_product_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  return $permissions;
}
