<?php
/**
 * @file
 * local_foodhub_commerce.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function local_foodhub_commerce_commerce_product_default_types() {
  $items = array(
    'foodhub_product' => array(
      'type' => 'foodhub_product',
      'name' => 'Foodhub product',
      'description' => 'A product sold through a local community foodhub.',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function local_foodhub_commerce_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "commerce_decimal_quantities" && $api == "default_commerce_decimal_quantities_presets") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function local_foodhub_commerce_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function local_foodhub_commerce_node_info() {
  $items = array(
    'foodhub_product_display' => array(
      'name' => t('Foodhub product display'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'order_cycle' => array(
      'name' => t('Order cycle'),
      'base' => 'node_content',
      'description' => t('A period during which foodhub orders can be made and delivered.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function local_foodhub_commerce_default_search_api_index() {
  $items = array();
  $items['foodhub_product_display_index'] = entity_import('search_api_index', '{
    "name" : "Foodhub product display index",
    "machine_name" : "foodhub_product_display_index",
    "description" : null,
    "server" : "database_server",
    "item_type" : "node",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "type" : { "type" : "string" },
        "title" : { "type" : "text" },
        "field_product_description" : { "type" : "text" },
        "search_api_language" : { "type" : "string" },
        "field_product:field_producer" : { "type" : "integer", "entity_type" : "node" },
        "field_product:field_category" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product:field_organic" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product:field_unit" : { "type" : "text" },
        "field_product:commerce_price:amount" : { "type" : "decimal" },
        "field_product:commerce_price:amount_decimal" : { "type" : "decimal" },
        "field_product:field_image:file" : { "type" : "integer", "entity_type" : "file" }
      },
      "data_alter_callbacks" : {
        "search_api_ranges_alter" : { "status" : 0, "weight" : "-50", "settings" : [] },
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "commerce_search_api_product_display_filter" : { "status" : 0, "weight" : "-10", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } }
      },
      "processors" : {
        "search_api_case_ignore" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : [],
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : { "fields" : [], "spaces" : "[^[:alnum:]]", "ignorable" : "[\\u0027]" }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : [],
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function local_foodhub_commerce_default_search_api_server() {
  $items = array();
  $items['database_server'] = entity_import('search_api_server', '{
    "name" : "Database server",
    "machine_name" : "database_server",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "1",
      "indexes" : {
        "foodhub_product_display_index" : {
          "search_api_language" : {
            "table" : "search_api_db_foodhub_product_display_index_search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_product:field_producer" : {
            "table" : "search_api_db_foodhub_product_display_index_field_product_field",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product:field_category" : {
            "table" : "search_api_db_foodhub_product_display_index_field_product_fie_1",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product:field_organic" : {
            "table" : "search_api_db_foodhub_product_display_index_field_product_fie_2",
            "type" : "integer",
            "boost" : "1.0"
          },
          "type" : {
            "table" : "search_api_db_foodhub_product_display_index_type",
            "type" : "string",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_foodhub_product_display_index_title",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_product_description" : {
            "table" : "search_api_db_foodhub_product_display_index_field_product_descr",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_product:field_unit" : {
            "table" : "search_api_db_foodhub_product_display_index_field_product_fie_3",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_product:commerce_price:amount_decimal" : {
            "table" : "search_api_db_foodhub_product_display_index_field_product_comme",
            "type" : "decimal",
            "boost" : "1.0"
          },
          "field_product:commerce_price:amount" : {
            "table" : "search_api_db_foodhub_product_display_index_field_product_com_1",
            "type" : "decimal",
            "boost" : "1.0"
          },
          "field_product:field_image:file" : {
            "table" : "search_api_db_foodhub_product_display_index_field_product_fie_4",
            "type" : "integer",
            "boost" : "1.0"
          }
        },
        "product_display" : {
          "nid" : {
            "table" : "search_api_db_product_display_nid",
            "type" : "integer",
            "boost" : "1.0"
          },
          "type" : {
            "table" : "search_api_db_product_display_type",
            "type" : "string",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_product_display_title",
            "type" : "string",
            "boost" : "1.0"
          },
          "status" : {
            "table" : "search_api_db_product_display_status",
            "type" : "integer",
            "boost" : "1.0"
          },
          "created" : {
            "table" : "search_api_db_product_display_created",
            "type" : "date",
            "boost" : "1.0"
          },
          "changed" : {
            "table" : "search_api_db_product_display_changed",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_tags" : {
            "table" : "search_api_db_product_display_field_tags",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_location_type" : {
            "table" : "search_api_db_product_display_field_location_type",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product" : {
            "table" : "search_api_db_product_display_field_product",
            "type" : "integer",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_product_display_search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "search_api_aggregation_1" : {
            "table" : "search_api_db_product_display_search_api_aggregation_1",
            "type" : "text",
            "boost" : "1.0"
          },
          "search_api_aggregation_2" : {
            "table" : "search_api_db_product_display_search_api_aggregation_2",
            "type" : "text",
            "boost" : "1.0"
          },
          "search_api_aggregation_3" : {
            "table" : "search_api_db_product_display_search_api_aggregation_3",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_tags:name" : {
            "table" : "search_api_db_product_display_field_tags_name",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          },
          "field_location_type:name" : {
            "table" : "search_api_db_product_display_field_location_type_name",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_product:field_category" : {
            "table" : "search_api_db_product_display_field_product_field_category",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product:field_organic" : {
            "table" : "search_api_db_product_display_field_product_field_organic",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product:commerce_price:amount_decimal" : {
            "table" : "search_api_db_product_display_field_product_commerce_price_amou",
            "type" : "decimal",
            "boost" : "1.0"
          },
          "field_product:field_category:name" : {
            "table" : "search_api_db_product_display_field_product_field_category_name",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_product:field_organic:name" : {
            "table" : "search_api_db_product_display_field_product_field_organic_name",
            "type" : "string",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
