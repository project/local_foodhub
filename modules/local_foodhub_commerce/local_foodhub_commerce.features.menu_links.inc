<?php
/**
 * @file
 * local_foodhub_commerce.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function local_foodhub_commerce_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:<front>
  $menu_links['main-menu:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Shop',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -10,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Shop');


  return $menu_links;
}
