<?php
/**
 * @file
 * local_foodhub_commerce.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function local_foodhub_commerce_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'foodhub-product-list';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'commerce_cart-cart' => array(
          'module' => 'commerce_cart',
          'delta' => 'cart',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'facetapi-bRBsZwX0t1khvvYOpnLUxYR34VVP135s' => array(
          'module' => 'facetapi',
          'delta' => 'bRBsZwX0t1khvvYOpnLUxYR34VVP135s',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'facetapi-teyIqfs1rfMOSeJfBEW0qu1iextZ1wIh' => array(
          'module' => 'facetapi',
          'delta' => 'teyIqfs1rfMOSeJfBEW0qu1iextZ1wIh',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'facetapi-wdiQQvPlRIZ3VBzNvNJJT1FPv1OSo0Xe' => array(
          'module' => 'facetapi',
          'delta' => 'wdiQQvPlRIZ3VBzNvNJJT1FPv1OSo0Xe',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['foodhub-product-list'] = $context;

  return $export;
}
