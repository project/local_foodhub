<?php
/**
 * @file
 * local_foodhub_commerce.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function local_foodhub_commerce_default_rules_configuration() {
  $items = array();
  $items['rules_close_order'] = entity_import('rules_config', '{ "rules_close_order" : {
      "LABEL" : "Close order",
      "PLUGIN" : "rule set",
      "TAGS" : [ "foodhub" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "close_order_node" : { "label" : "node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "close-order-node" ],
                  "type" : { "value" : { "order_cycle" : "order_cycle" } }
                }
              }
            ],
            "DO" : [
              { "data_set" : {
                  "data" : [ "close-order-node:field-order-cycle-status" ],
                  "value" : "Delivery only"
                }
              }
            ],
            "LABEL" : "Close order cycle rule"
          }
        }
      ]
    }
  }');
  $items['rules_close_order_cycle'] = entity_import('rules_config', '{ "rules_close_order_cycle" : {
      "LABEL" : "Close order cycle",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "foodhub" ],
      "REQUIRES" : [ "rules", "rules_scheduler" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "order_cycle" : "order_cycle" } }
          }
        }
      ],
      "DO" : [
        { "schedule" : {
            "component" : "rules_close_order",
            "date" : {
              "select" : "node:field-start-date",
              "date_offset" : { "value" : 86400 }
            },
            "identifier" : "Close for delivery [node:nid]",
            "param_close_order_node" : [ "node" ]
          }
        }
      ]
    }
  }');
  $items['rules_foodhub_markup'] = entity_import('rules_config', '{ "rules_foodhub_markup" : {
      "LABEL" : "Foodhub markup",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "foodhub" ],
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference" ],
      "ON" : [ "commerce_product_calculate_sell_price" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "2" : "2", "3" : "3", "4" : "4", "5" : "5", "6" : "6" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "commerce_line_item_unit_price_multiply" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : "1.1",
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        }
      ]
    }
  }');
  $items['rules_notify_producers'] = entity_import('rules_config', '{ "rules_notify_producers" : {
      "LABEL" : "Notify producers",
      "PLUGIN" : "rule",
      "TAGS" : [ "foodhub" ],
      "REQUIRES" : [
        "rules",
        "rb_misc",
        "views_bulk_operations",
        "foodhub",
        "php",
        "mimemail"
      ],
      "USES VARIABLES" : { "order_cycle" : { "label" : "Order cycle", "type" : "node" } },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "order-cycle" ],
            "type" : { "value" : { "order_cycle" : "order_cycle" } }
          }
        }
      ],
      "DO" : [
        { "rb_misc_action_views_load_list" : {
            "USING" : { "view" : "business|default" },
            "PROVIDE" : { "node_list" : { "business_list" : "Business list" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "business-list" ] },
            "ITEM" : { "business" : "Business" },
            "DO" : [
              { "views_bulk_operations_action_load_list" : {
                  "USING" : {
                    "view" : "producer_order_cycle_product_list|default",
                    "args" : "[order-cycle:nid]\\r\\n[business:nid]"
                  },
                  "PROVIDE" : { "entity_list" : { "producer_order_cycle_list" : "Producer order cycle list" } }
                }
              },
              { "foodhub_format_line_items" : {
                  "USING" : { "commerce_line_item_list" : [ "producer-order-cycle-list" ] },
                  "PROVIDE" : { "line_items_html" : { "line_items_html" : "Line items" } }
                }
              },
              { "mimemail" : {
                  "to" : "pauljmackay@gmail.com",
                  "subject" : "A Totnes FoodHUB order has been placed.",
                  "body" : "\\u003Chtml\\u003E\\r\\n\\u003Chead\\u003E\\u003C\\/head\\u003E\\r\\n\\u003Cbody\\u003E\\r\\n\\r\\nThe order started on [order-cycle:field-start-date]. The produce will be delivered on the following Saturday.\\r\\n\\r\\nThe combined order is:\\r\\n\\u003C?php\\r\\necho $line_items_html;\\r\\n?\\u003E\\r\\n\\r\\n\\u003C\\/body\\u003E\\r\\n\\u003C\\/html\\u003E"
                }
              }
            ]
          }
        },
        { "drupal_message" : { "message" : "[order-cycle:field_start_date]" } }
      ]
    }
  }');
  $items['rules_send_invoice_receipt_email'] = entity_import('rules_config', '{ "rules_send_invoice_receipt_email" : {
      "LABEL" : "Send invoice receipt email",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "foodhub" ],
      "REQUIRES" : [ "rules", "commerce_invoice_receipt", "commerce_checkout" ],
      "ON" : [ "commerce_checkout_complete" ],
      "DO" : [
        { "commerce_invoice_receipt_action_mail" : {
            "commerce_order" : [ "commerce_order" ],
            "to" : "[commerce-order:owner] \\u003C[commerce-order:mail]\\u003E",
            "subject" : "Totnes Food HUB: [commerce-order:field_order_cycle] - [commerce-order:order-id]"
          }
        }
      ]
    }
  }');
  return $items;
}
