<?php
/**
 * @file
 * local_foodhub_commerce.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function local_foodhub_commerce_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-order-cycle-actions.
  $menus['menu-order-cycle-actions'] = array(
    'menu_name' => 'menu-order-cycle-actions',
    'title' => 'Order cycle actions',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Main menu');
  t('Order cycle actions');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');


  return $menus;
}
