<?php
/**
 * @file
 * local_foodhub_commerce.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function local_foodhub_commerce_user_default_roles() {
  $roles = array();

  // Exported role: coordinator.
  $roles['coordinator'] = array(
    'name' => 'coordinator',
    'weight' => 7,
  );

  return $roles;
}
